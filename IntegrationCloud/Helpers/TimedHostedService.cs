﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Common.Helpers
{
    internal abstract class TimedHostedService : IHostedService, IDisposable
    {
        protected internal const int DueTime = 15000; // milliseconds
        protected readonly ILogger Logger;
        private Timer _timer;
        private bool _running;
        static bool _timerRunning = true;

        protected TimedHostedService(ILogger<TimedHostedService> logger)
        {
            Logger = logger;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Logger.LogInformation("Timed Background Service is starting.");
            _running = true;
            _timer = new Timer(DoWorkInternal, null, TimeSpan.Zero,
                TimeSpan.FromSeconds(DueTime));

            return Task.CompletedTask;
        }

        private void DoWorkInternal(object state)
        {
            try
            {
                if (_timerRunning)
                {
                    Logger.LogInformation("Timed Background Service is working.");
                    _timerRunning = false;
                    _timer?.Change(Timeout.Infinite, Timeout.Infinite);
                    DoWork(state);
                }
            }
            finally
            {
                _timerRunning = true;
                if (_running)
                    _timer?.Change(DueTime, DueTime);
            }
        }

        protected abstract void DoWork(object state);

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _running = false;
            Logger.LogInformation("Timed Background Service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
