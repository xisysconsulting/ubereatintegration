﻿

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using UberEatClient.Models;

namespace Common.Helpers
{
    public class RestClient
    {
        #region Fields

        private readonly HttpClient client = new HttpClient();

        #endregion Fields

        #region Constructor

        public RestClient()
        {
        }

        public RestClient(string baseAddress, string authToken)
        {
            if (!string.IsNullOrWhiteSpace(baseAddress))
                client.BaseAddress = new Uri(baseAddress);
            if (!string.IsNullOrWhiteSpace(authToken))
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", authToken);

        }
        #endregion Constructor

        #region Public Methods

        public async Task<string> GetStringAsync(string api)
        {
            var res = await client.GetAsync(api);
            res.EnsureSuccessStatusCode();
            var resultString = await res.Content.ReadAsStringAsync();
            return resultString;
        }

        public async Task<T> GetAsync<T>(string api, ClientHeaders clientHeaders)
        {
            SetClientHeaders(clientHeaders);

            var res = await client.GetAsync(api);
            res.EnsureSuccessStatusCode();

            var result = await res.Content.ReadAsAsync<T>();
            return result;
        }

        public async Task<T> PatchAsync<T>(string api, object content, ClientHeaders clientHeaders)
        {
            SetClientHeaders(clientHeaders);

            var jsonRequest = JsonConvert.SerializeObject(content);
            var httpContent = new StringContent(jsonRequest, Encoding.UTF8, "application/json-patch+json");

            HttpResponseMessage response = await client.PatchAsync(api, httpContent);
            response.EnsureSuccessStatusCode();
            var res = await response.Content.ReadAsAsync<T>();
            return res;
        }

        public async Task<T> PostAsyncAsJsonExt<T>(string api, object content, ClientHeaders clientHeaders)
        {
            SetClientHeaders(clientHeaders);

            HttpResponseMessage response = await client.PostAsJsonExtAsync(api, content);
            response.EnsureSuccessStatusCode();
            var res = await response.Content.ReadAsAsync<T>();
            return res;
        }

        public async Task<T> PostAsyncAsJson<T>(string api, HttpContent content, ClientHeaders clientHeaders)
        {
            SetClientHeaders(clientHeaders);
            // // client.DefaultRequestHeaders.Add(Rest_Constants.Content_Type, Rest_Constants.CONTENT_UrlEncoded);
            HttpResponseMessage response = await client.PostAsync(api, content);
            response.EnsureSuccessStatusCode();
            var res = await response.Content.ReadAsAsync<T>();
            return res;
        }

        public async Task<T> PutAsync<T>(string api, object content, ClientHeaders clientHeaders)
        {
            SetClientHeaders(clientHeaders);

            HttpResponseMessage response = await client.PutAsJsonExtAsync(api, content);

            if (response.IsSuccessStatusCode)
            {
                response.EnsureSuccessStatusCode();

                var res = await response.Content.ReadAsAsync<T>();
                return res;
            }
            else
            {
                return default(T);
            }
        }

        private void SetClientHeaders(ClientHeaders clientHeaders)
        {
            if (clientHeaders != null)
            {
                foreach (var itm in clientHeaders.Headers)
                {
                    client.DefaultRequestHeaders.TryAddWithoutValidation(itm.Key, itm.Value);
                }
            }
        }

        public async Task<bool> DeleteAsync(string api, string id)
        {
            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", Rest_Constants.CONNECT_PYSNSE_VER);
            HttpResponseMessage response = await client.DeleteAsync(api + $"/{id}");
            return response.IsSuccessStatusCode;

        }

        public async Task<bool> DeleteAsync(string api, object content)
        {

            HttpResponseMessage response = await client.DeleteAsJsonExtAsync(api, content);
            response.EnsureSuccessStatusCode();
            return true;
        }

        T GetObjectFromStream<T>(Stream stream)
        {
            var serializer = new DataContractJsonSerializer(typeof(T));
            var res = (T)serializer.ReadObject(stream);
            return res;
        }

        public HttpContent ObjectToContent(object content)
        {
            var myContent = JsonConvert.SerializeObject(content);
            var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return byteContent;

        }
        public void AddHeader(string key, string value)
        {
            client.DefaultRequestHeaders.TryAddWithoutValidation(key, value);
        }


        #endregion Public Methods

    }

    public static class HttpClientExtension
    {

        /// <summary>
        /// Extended version of POST
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="client"></param>
        /// <param name="api"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> PostAsJsonExtAsync<T>(this HttpClient client, string api, T content)
        {
            var formatter = new JsonMediaTypeFormatter();
            formatter.SerializerSettings = new JsonSerializerSettings
            {
                //Formatting = Formatting.Indented,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
            HttpResponseMessage response = await client.PostAsync(api, content, formatter);

            return response;
        }

        /// <summary>
        /// Extended version of PUT
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="client"></param>
        /// <param name="api"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> PutAsJsonExtAsync<T>(this HttpClient client, string api, T content)
        {
            var formatter = new JsonMediaTypeFormatter();
            formatter.SerializerSettings = new JsonSerializerSettings
            {
                //Formatting = Formatting.Indented,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
            HttpResponseMessage response = await client.PutAsync(api, content, formatter);

            return response;
        }


        /// <summary>
        /// Extended version of Delete
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="client"></param>
        /// <param name="api"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> DeleteAsJsonExtAsync<T>(this HttpClient client, string api, T content)
        {
            var jsonSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
            var request = new HttpRequestMessage(HttpMethod.Delete, api);
            request.Content = new StringContent(JsonConvert.SerializeObject(content, jsonSerializerSettings), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.SendAsync(request);

            return response;
        }

    }

    public static class UberEatHelper
    {
        public async static Task<bool> PutMenu(object data, string token, string storeId)
        {
            ClientHeaders clientHeaders = new ClientHeaders();
            clientHeaders.Headers.Add(Rest_Constants.Authorization, String.Format(Uber_Constants.Authorization, token));
            clientHeaders.Headers.Add(Rest_Constants.Content_Type, Rest_Constants.CONTENT_JSON);

            return await new RestClient().PutAsync<bool>(String.Format(Uber_Constants.MenuPutUri, storeId), data, clientHeaders);

        }

        public async static Task<UberEatOrder> GetOrderDetails(string resourse_location, string token)
        {
            ClientHeaders clientHeaders = new ClientHeaders();
            clientHeaders.Headers.Add(Rest_Constants.Authorization, String.Format(Uber_Constants.Authorization, token));
            clientHeaders.Headers.Add(Rest_Constants.Content_Type, Rest_Constants.CONTENT_JSON);

            return await new RestClient().GetAsync<UberEatOrder>(resourse_location, clientHeaders);
        }

        public async static Task PostOrderStatus(string order_id, OrderAccept orderAccept, OrderDenial orderDenial, string token)
        {
            ClientHeaders clientHeaders = new ClientHeaders();
            clientHeaders.Headers.Add(Rest_Constants.Authorization, String.Format(Uber_Constants.Authorization, token));
            clientHeaders.Headers.Add(Rest_Constants.Content_Type, Rest_Constants.CONTENT_JSON);

            if (orderAccept != null)
            {
                await new RestClient().PostAsyncAsJsonExt<string>(String.Format(Uber_Constants.OrderAcceptUri, order_id), orderAccept, clientHeaders);
            }
            else if (orderDenial.reason != null)
            {
                await new RestClient().PostAsyncAsJsonExt<string>(String.Format(Uber_Constants.OrderDenyUri, order_id), orderDenial, clientHeaders);
            }

        }

        public async static Task<UberEatStoreStatus> GetStoreStatus(string storeGuid, string token)
        {
            ClientHeaders clientHeaders = new ClientHeaders();
            clientHeaders.Headers.Add(Rest_Constants.Authorization, String.Format(Uber_Constants.Authorization, token));
            return await new RestClient().GetAsync<UberEatStoreStatus>(String.Format(Uber_Constants.StoreStatus, storeGuid), clientHeaders);
        }

        public async static Task<UberEatStore> GetStoreInfo(string storeGuid, string token)
        {
            ClientHeaders clientHeaders = new ClientHeaders();
            clientHeaders.Headers.Add(Rest_Constants.Authorization, String.Format(Uber_Constants.Authorization, token));
            clientHeaders.Headers.Add(Rest_Constants.Content_Type, Rest_Constants.CONTENT_JSON);

            return await new RestClient().GetAsync<UberEatStore>(String.Format(Uber_Constants.StoreDetail, storeGuid), clientHeaders);
        }

        public async static Task PatchStoreStatus(string storeGuid, StoreStatusRequest storeStatusRequest, string token)
        {
            ClientHeaders clientHeaders = new ClientHeaders();
            clientHeaders.Headers.Add(Rest_Constants.Authorization, String.Format(Uber_Constants.Authorization, token));
            clientHeaders.Headers.Add(Rest_Constants.Content_Type, Rest_Constants.CONTENT_JSON);

            await new RestClient().PatchAsync<string>(String.Format(Uber_Constants.StoreStatus, storeGuid), storeStatusRequest, clientHeaders);
        }

        public async static Task<UberAccessToken> GetToken(string clientId, string clientSecret, string scope)
        {
            List<KeyValuePair<string, string>> postData = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>(Uber_Constants.TokenCltId, clientId),
                new KeyValuePair<string, string>(Uber_Constants.TokenCltScrt, clientSecret),
                new KeyValuePair<string, string>(Uber_Constants.TokenGrntTypKey, Uber_Constants.TokenCltCrdVal),
                new KeyValuePair<string, string>(Uber_Constants.TokenScopeKey, scope) // eats.store, eats.order
            };

            var contant = new FormUrlEncodedContent(postData);

            return await new RestClient().PostAsyncAsJson<UberAccessToken>(Uber_Constants.TokenUri, contant, null);
        }

    }

    public static class EposHubHelper
    {
        public async static Task<string> SendCall(string baseURL, string methodAndOrParamerters, object bodyObject)
        {
            ClientHeaders clientHeaders = new ClientHeaders();
            clientHeaders.Headers.Add(Rest_Constants.Content_Type, Rest_Constants.CONTENT_JSON);
            var payload = JsonConvert.SerializeObject(bodyObject);

            return await new RestClient().PostAsyncAsJsonExt<string>(baseURL + ((string.IsNullOrEmpty(methodAndOrParamerters)) ? "" : methodAndOrParamerters), payload, clientHeaders);
        }

        public async static Task<string> SendCall(string baseURL, string method, object parameter, string test = "")
        {
            try
            {
                string requestUrl = baseURL;

                var payload = JsonConvert.SerializeObject(parameter);

                var result = await SendCallBase(requestUrl, method, payload, null, "POST");

                if (!string.IsNullOrWhiteSpace(result))
                    return result;

                return "-1";
            }
            catch { return "-1"; }
        }

        private static async Task<string> SendCallBase(string baseURL, string methodAndOrParamerters, object bodyObject, Dictionary<string, string> headers, string type)
        {
            if (string.IsNullOrEmpty(baseURL)) return "ERROR";

            try
            {
                string payload = "";
                if (bodyObject != null)
                {
                    if (bodyObject is string)
                    {
                        payload = bodyObject.ToString();
                    }
                    else
                    {
                        payload = JsonConvert.SerializeObject(bodyObject);
                    }
                }
                WebClient client = new WebClient();
                if (headers?.Count > 0)
                {
                    foreach (var header in headers)
                    {
                        client.Headers.Add(header.Key, header.Value);
                    }
                }

                string result;

                if (type.ToLower() == "get")
                {
                    var uri = baseURL + (string.IsNullOrEmpty(methodAndOrParamerters) ? "" : "/" + methodAndOrParamerters);
                    result = await client.DownloadStringTaskAsync(uri);
                    int x = 3;
                }
                else
                {
                    var asdasd = await client.UploadDataTaskAsync(baseURL + ((string.IsNullOrEmpty(methodAndOrParamerters)) ? "" : "/" + methodAndOrParamerters), type.ToString(),
                        Encoding.Default.GetBytes(payload));
                    int x = 5;
                    result = Encoding.ASCII.GetString(asdasd).Replace(@"\", "").Replace(@"\", "");
                }

                return result;
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }
    }


    public class ClientHeaders
    {
        public Dictionary<string, string> Headers = new Dictionary<string, string>();
    }

}