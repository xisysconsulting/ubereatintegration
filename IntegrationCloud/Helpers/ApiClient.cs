﻿/*
 
Extended version of Tim Daughton's EposHub/ProviderApiClient.cs

*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace UberEatClient.Services
{
    public static class ApiClient
    {
        public static async Task<T> CallApi<T>(string baseURL, string methodAndOrParamerters, object bodyObject, Dictionary<string, string> headers, RestApiCallType type, bool isUploadImage)
        {

            string result = "";

            if (isUploadImage)
                result = await UploadImage(baseURL, methodAndOrParamerters, (byte[])bodyObject, headers, type);
            else
                result = await SendCall(baseURL, methodAndOrParamerters, bodyObject, headers, type);

            if (result != "ERROR")
            {
                return JsonConvert.DeserializeObject<T>(result);
            }

            return default(T);
        }

        public static async Task<bool> CallApi(string baseURL, string methodAndOrParamerters, object bodyObject, Dictionary<string, string> headers, RestApiCallType type)
        {
            if (await SendCall(baseURL, methodAndOrParamerters, bodyObject, headers, type) != "ERROR")
            {
                return true;
            }

            return false;
        }

        private static async Task<string> UploadImage(string baseURL, string methodAndOrParamerters, byte[] bodyObject, Dictionary<string, string> headers, RestApiCallType type)
        {
            if (string.IsNullOrEmpty(baseURL)) return "ERROR";
            if (bodyObject == null) return "ERROR";

            try
            {
                WebClient client = new WebClient();

                if (headers?.Count > 0)
                {
                    foreach (var header in headers)
                    {
                        client.Headers.Add(header.Key, header.Value);
                    }
                }

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                                                        SecurityProtocolType.Tls11 |
                                                        SecurityProtocolType.Tls12 |
                                                        SecurityProtocolType.Ssl3;

                string result;

                //client.UploadDataCompleted += Client_UploadDataCompleted;
                result = Encoding.ASCII.GetString(await client.UploadDataTaskAsync(baseURL + ((string.IsNullOrEmpty(methodAndOrParamerters)) ? "" : methodAndOrParamerters), type.ToString(), bodyObject));

                return result;
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }


        private static async Task<string> SendCall(string baseURL, string methodAndOrParamerters, object bodyObject, Dictionary<string, string> headers, RestApiCallType type)
        {
            if (string.IsNullOrEmpty(baseURL)) return "ERROR";

            try
            {
                string payload = "";

                if (bodyObject != null)
                {
                    payload = JsonConvert.SerializeObject(bodyObject);
                }

                WebClient client = new WebClient();

                if (headers?.Count > 0)
                {
                    foreach (var header in headers)
                    {
                        client.Headers.Add(header.Key, header.Value);
                    }
                }

                try
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                                                            SecurityProtocolType.Tls11 |
                                                            SecurityProtocolType.Tls12 |
                                                            SecurityProtocolType.Ssl3;
                }
                catch (Exception ex)
                {
                    // ignore security error
                }
                string result;

                if (type == RestApiCallType.GET)
                {
                    var uri = baseURL + (string.IsNullOrEmpty(methodAndOrParamerters) ? "" : "/" + methodAndOrParamerters);
                    result = await client.DownloadStringTaskAsync(uri);
                    // result = Encoding.ASCII.GetString(res);
                    int x = 3;
                }
                ////else if (type == RestApiCallType.PUT)
                ////{
                ////    var restClient = new RestClient(baseURL);
                ////    var rcRequest = new RestRequest(((string.IsNullOrEmpty(methodAndOrParamerters)) ? "" : "/" + methodAndOrParamerters), Method.PUT)
                ////    {
                ////        RequestFormat = DataFormat.Json
                ////    };
                ////    //request.AddBody(new { @null = string.Empty });
                ////    rcRequest.AddJsonBody(bodyObject);
                ////    foreach (var header in headers)
                ////    {
                ////        restClient.AddDefaultHeader(header.Key, header.Value);
                ////    }

                ////    var rcResponse = restClient.Put(rcRequest);

                ////    if (rcResponse.IsSuccessful)
                ////    {
                ////        result = rcResponse.Content;
                ////    }
                ////    else
                ////    {
                ////        result = "ERROR";
                ////    }
                ////}
                else
                {
                    //client.UploadDataCompleted += Client_UploadDataCompleted;
                    var asdasd = await client.UploadDataTaskAsync(baseURL + ((string.IsNullOrEmpty(methodAndOrParamerters)) ? "" : "/" + methodAndOrParamerters), type.ToString(), Encoding.Default.GetBytes(payload));
                    int x = 5;
                    result = Encoding.ASCII.GetString(asdasd);
                    //result = Encoding.ASCII.GetString(await client.UploadDataTaskAsync(baseURL + ((string.IsNullOrEmpty(methodAndOrParamerters)) ? "" : methodAndOrParamerters), type.ToString(), Encoding.Default.GetBytes(payload)));

                }

                return result;
            }
            catch (Exception ex)
            {
                return "ERROR";
            }
        }

        //private static void Client_UploadDataCompleted(object sender, UploadDataCompletedEventArgs e)
        //{
        //    int x = 5;
        //}
    }

    public enum RestApiCallType
    {
        GET,
        POST,
        PUT,
        DELETE
    }
}
