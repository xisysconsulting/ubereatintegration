﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.Helpers
{
    public class Rest_Constants
    {
        public const string CONTENT_JSON = "application/json";
        public const string CONTENT_UrlEncoded = "application/x-www-form-urlencoded";
        public const string CONNECT_PYSNSE_VER = "application/connect.v1+json";
        public const string Authorization = "Authorization";
        public const string Content_Type = "Content-Type";

    }

    public class Uber_Constants
    {
        public const string GBP_Short = "GBP";
        public const string Epos_Service = "http://localhost:8123/EposHub.SssPosWebHooks.svc/";
        public const string AuthTokenEatOrder = "JA.VUNmGAAAAAAAEgASAAAABwAIAAwAAAAAAAAAEgAAAAAAAAHQAAAAFAAAAAAADgAQAAQAAAAIAAwAAAAOAAAApAAAABwAAAAEAAAAEAAAACzXcDU4KdIOqoM1H3Vr9l2AAAAAL3_CacMnge_gMwAwnYnvvJ_HFmgCBKMkdunkxS3lfe8XEY_TCGCt8kNRDOX2d0SSSx6GiUd2qeRLwkfm2lzyGjTv-XOdEdyBXKFYwX41MGNdoYx109GtiTBycATll_luHEE03FS9jlkKiYx6CCbi0Tk1I3pQsSPKirmNpvnIc7QMAAAAsKOaQcpE3FF-wAwEJAAAAGIwZDg1ODAzLTM4YTAtNDJiMy04MDZlLTdhNGNmOGUxOTZlZQ";
        public const string AuthTokenEatStore = "JA.VUNmGAAAAAAAEgASAAAABwAIAAwAAAAAAAAAEgAAAAAAAAHQAAAAFAAAAAAADgAQAAQAAAAIAAwAAAAOAAAApAAAABwAAAAEAAAAEAAAADpfzktXRZaqaSEdikucSDCAAAAAgoBVcWJNAgRhElqFymH9931T6W77_tJCiUa-1oiZq8PAMAWjzPFGPnZqQAFiXK3fXqqNNRGxUces4G1uX-_6AlrQcBNUBhBZ-NmV2-RIya8HYQFHKSU8WblY06yhvKQ7YqTt7zFMUJgXQGEQanziDLidFYGoiFQkSbd3w6ucSyIMAAAA8C4RQY3-iynqh4WtJAAAAGIwZDg1ODAzLTM4YTAtNDJiMy04MDZlLTdhNGNmOGUxOTZlZQ";

        public const string Breakfast = "Breakfast";
        public const string MainCourse = "MainCourse";
        public const string Drinks = "Drinks";

        public const string MenuPutUri = "https://api.uber.com/v1/eats/stores/{0}/menu";
        public const string OrderGetUri = "https://api.uber.com/v1/eats/orders/{0}";
        public const string TokenUri = "https://login.uber.com/oauth/v2/token";

        public const string TokenCltId = "client_id";
        public const string TokenCltScrt = "client_secret";
        public const string TokenGrntTypKey = "grant_type";
        public const string TokenCltCrdVal = "client_credentials";
        public const string TokenScopeKey = "scope";
        public const string TokenScpOrdVal = "eats.order";
        public const string TokenScpStorVal = "eats.store";


        public const string Authorization = "Bearer {0}";

        public const string OrderAcceptReason = "Order has been accepted.";
        public const string OrderAcceptUri = "https://api.uber.com/v1/eats/orders/{0}/accept_pos_order";
        public const string OrderDenyReason = "Order has been denied. One or more items are sold out.";
        public const string OrderDenyUri = "https://api.uber.com/v1/eats/orders/{0}/deny_pos_order";

        public const string StoreStatus = "https://api.uber.com/v1/eats/stores/{0}/pos_data";
        public const string StoreDetail = "https://api.uber.com/v1/eats/stores/{0}";

        public const string EventOrderNotification = "orders.notification";

        public const string OrderStateCurrent = "created";
        public const string OrderStateAccepted = "accepted";
        public const string OrderStateDenied = "denied";
        public const string OrderStateUnknown = "unknown";
        public const string OrderStateDuplicate = "duplicate";

    }

}
