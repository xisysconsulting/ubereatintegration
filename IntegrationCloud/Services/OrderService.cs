﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Common.EposHubDb;
using Common.Helpers;
using UberEatClient.Models;

namespace UberEatClient.Services
{

    public class OrderService
    {
        private static bool isProcessingOrders = false;
        private readonly Common.EposHubDb.EposHubDbContext _eposHubDbContext;
        private readonly Common.EposAdmin.EposAdminContext _eposAdminContext;
        private readonly UberEatCustomer _uberEatCustomer;
        private readonly Settings _settings;
        private readonly TokensService _tokensService;

        public OrderService(Common.EposHubDb.EposHubDbContext eposHubDb, Common.EposAdmin.EposAdminContext eposAdminContext,
            IOptions<UberEatCustomer> uberEatCustomer, IOptions<Settings> settings, TokensService tokensService)
        {
            _eposHubDbContext = eposHubDb;
            _uberEatCustomer = uberEatCustomer.Value;
            _eposAdminContext = eposAdminContext;
            _settings = settings.Value;
            _tokensService = tokensService;
        }

        public async Task<UberEatStoreStatus> GetStoreStatus(string storeId)
        {
            var uberEatSetting = _eposHubDbContext.UberEatSettings.Where(s => s.UberEatStoreId.Equals(storeId)).FirstOrDefault();
            if (uberEatSetting != null)
            {
                UberEatStoreStatus uberStoreStatus = await UberEatHelper.GetStoreStatus(storeId, _tokensService.GetTokenStore(uberEatSetting));
                return uberStoreStatus;
            }
            return null;
        }

        public async Task<UberEatStore> GetStoreDetails(string storeId)
        {
            var uberEatSetting = _eposHubDbContext.UberEatSettings.Where(s => s.UberEatStoreId.Equals(storeId)).FirstOrDefault();
            if (uberEatSetting != null)
            {
                UberEatStore uberStore = await UberEatHelper.GetStoreInfo(storeId, _tokensService.GetTokenStore(uberEatSetting));
                return uberStore;
            }
            return null;
        }

        public async Task SetStoreStatus(string storeId, bool on)
        {
            StoreStatusRequest storeStatusRequest = new StoreStatusRequest();
            var uberEatSetting = _eposHubDbContext.UberEatSettings.Where(s => s.UberEatStoreId.Equals(storeId)).FirstOrDefault();
            if (uberEatSetting != null)
            {
                if (on)
                {
                    storeStatusRequest.pos_integration_enabled = true;
                }
                await UberEatHelper.PatchStoreStatus(storeId, storeStatusRequest, _tokensService.GetTokenStore(uberEatSetting));
            }
        }

        public async Task ProcessNotification(UberEatWebhootNotification data)
        {
            if (data.event_type.Equals(Uber_Constants.EventOrderNotification))
            {
                var uberEatNotifications = _eposHubDbContext.UberEatNotifications.Where(s => s.EventId == data.event_id).FirstOrDefault();
                if (uberEatNotifications == null)
                {
                    await AddNotification(data, false);
                }
                else
                {
                    // // To Record event callbacks of UberEat for futher analysis of uber data, in case of update, cancel order
                    await AddNotification(data, true);
                }
            }
            await ProcessOrders();
        }

        public async Task ProcessOrders()
        {
            try
            {
                if (!isProcessingOrders)
                {
                    isProcessingOrders = true;
                    var NotificationData = _eposHubDbContext.UberEatNotifications.Where(s => s.IsOrderProcess == false).ToList();

                    if (NotificationData != null && NotificationData.Count > 0)
                    {
                        foreach (var notification in NotificationData)
                        {
                            try
                            {
                                string hubriseReason = null;
                                var hubriseReasonDenied = new UberReason();
                                var uberAccept = new OrderAccept();
                                var uberDenied = new OrderDenial();
                                var metaData = JsonConvert.DeserializeObject<OrderMeta>(notification.Meta);

                                if (notification.EventType.Equals(Uber_Constants.EventOrderNotification))
                                {
                                    var uberEatSetting = _eposHubDbContext.UberEatSettings.Where(s => s.UberEatStoreId.Equals(metaData.user_id)).FirstOrDefault();

                                    if (uberEatSetting != null && uberEatSetting.ProcessOrder)
                                    {
                                        // post it to epos hub
                                        long hubRiseOrderID = 0;
                                        string strToken = _tokensService.GetTokenOrder(uberEatSetting);

                                        try
                                        {
                                            UberEatOrder uberEatOrder = null; // await UberEatHelper.GetOrderDetails(notification.ResourceHref, strToken);
                                            try
                                            {
                                                uberEatOrder = await UberEatHelper.GetOrderDetails(notification.ResourceHref, strToken);
                                            }
                                            catch (Exception err)
                                            {   // // may be token expired, lets do another try after refeshing token
                                                //_tokensService.GetTokenOrder()
                                                hubriseReason = err.Message;
                                                if (err.Message.Contains("Unauthorized"))
                                                {
                                                    strToken = _tokensService.GetTokenOrder(uberEatSetting, true);
                                                    uberEatOrder = await UberEatHelper.GetOrderDetails(notification.ResourceHref, strToken);

                                                }
                                            }
                                            if (uberEatOrder != null)
                                            {
                                                string result = await HubRiseProcessOrder(uberEatOrder, uberEatSetting);
                                                result = result.Replace("\"", "").Replace("\\", "").Replace(@"""\", "");
                                                hubRiseOrderID = (long)Convert.ToDouble(result);
                                            }
                                        }
                                        catch (Exception err)
                                        {
                                            hubriseReason = err.Message;
                                        }

                                        if (hubRiseOrderID > 0)
                                        {
                                            hubriseReason = Uber_Constants.OrderAcceptReason;
                                            uberAccept = new OrderAccept() { reason = hubriseReason };
                                            await UberEatHelper.PostOrderStatus(metaData.resource_id, uberAccept, null, strToken);
                                            notification.Status = Uber_Constants.OrderStateAccepted;
                                            notification.IsOrderProcess = true;
                                        }
                                        else
                                        {
                                            if (notification.Count < _settings.checkOrderNotificationCount)
                                            {
                                                notification.IsOrderProcess = false;
                                            }
                                            else
                                            {
                                                notification.IsOrderProcess = true;
                                                if (!string.IsNullOrEmpty(hubriseReason))
                                                {
                                                    hubriseReasonDenied = new UberReason() { explanation = Uber_Constants.OrderDenyReason + " " + hubriseReason };
                                                    notification.Status = Uber_Constants.OrderStateDenied + " " + hubriseReason;
                                                }
                                                else
                                                {
                                                    hubriseReasonDenied = new UberReason() { explanation = Uber_Constants.OrderDenyReason };
                                                    notification.Status = Uber_Constants.OrderStateDenied;
                                                }
                                                uberDenied = new OrderDenial() { reason = hubriseReasonDenied };
                                                await UberEatHelper.PostOrderStatus(metaData.resource_id, null, uberDenied, strToken);
                                            }
                                        }
                                        await updateNotification(notification);
                                    }  // // erEatSetting != null
                                }
                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    if (notification.Count > _settings.checkOrderNotificationCount)
                                    {
                                        notification.IsOrderProcess = true;
                                    }
                                    notification.Status += " " + ex.Message;
                                    await updateNotification(notification);
                                }
                                catch (Exception)
                                {
                                    // // ignore individual db foreach (var notification 
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // // ignore indvidual errors
            }
            finally
            {
                isProcessingOrders = false;
            }
        }

        private async Task<string> HubRiseProcessOrder(UberEatOrder eatOrder, Common.EposHubDb.UberEatSetting uberEatSetting)
        {
            string errorText = "";
            bool isSSk = _uberEatCustomer.isSSk;
            string phone = "";
            string lastName = "";
            int total = 0;
            try
            {
                var station = _eposHubDbContext.Stations.Where(s => s.ClientUd == uberEatSetting.ClientUD).FirstOrDefault();
                var clients = _eposAdminContext.Clients.Where(s => s.ClientId == station.ClientUd).FirstOrDefault();

                Guid admincompanyud = clients.CompanyId;

                errorText = " Order " + eatOrder.order_num;

                string orderName = eatOrder.order_num + "_";

                SPosOrderEvent sPosOrderEvent = new SPosOrderEvent
                {
                    location_id = station.ProviderLocationId,
                    order = new SPosOrder
                    {
                        order_id = eatOrder.order_num,
                        name = orderName,
                        asap = "true",
                        currency = eatOrder.currency_code,
                        items = new List<SPosItem>(),
                        OrderJSON = JsonConvert.SerializeObject(eatOrder),
                        is_table = false,
                        is_collection = false,
                        is_paid = true
                    },
                    acknowledged_at = "",
                    payment_records = new List<SPosPayment>(),
                    customer = null

                };

                decimal paymentRecordsSum = 0;

                foreach (var charge in eatOrder.charges)
                {
                    total += Convert.ToInt32(charge.price);
                }

                foreach (var uberItem in eatOrder.order_items)
                {
                    if (string.IsNullOrEmpty(uberItem.external_data)) continue;
                    errorText = " product " + uberItem.external_data;

                    var posI = GetPosItemforUberItem(uberItem, eatOrder.currency_code);
                    foreach (var mod in uberItem.selected_options)
                    {
                        if (string.IsNullOrEmpty(mod.external_data)) continue;
                        posI.modifiers.Add(GetSPosModifierforUberOption(mod, eatOrder.currency_code));
                        int price = (uberItem.price == null) ? 0 : (int)(Convert.ToDecimal(uberItem.price.Replace(" " + eatOrder.currency_code, string.Empty)));
                        paymentRecordsSum += (Convert.ToDecimal(price) * Convert.ToDecimal(uberItem.quantity));
                    }
                    int ProductPrice = (uberItem.price == null) ? 0 : (int)(Convert.ToDecimal(uberItem.price.Replace(" " + eatOrder.currency_code, string.Empty)));
                    paymentRecordsSum += (Convert.ToDecimal(ProductPrice) * Convert.ToDecimal(uberItem.quantity));

                    sPosOrderEvent.order.items.Add(posI);
                }

                sPosOrderEvent.order.total = (int)total;

                var hurRiseStatus = await ServicePost("ProcessOnlineOrder", sPosOrderEvent, uberEatSetting.EposhubService);

                return hurRiseStatus;
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorText))
                    throw new Exception(ex.Message);
                else
                    throw new Exception("Correct your order for " + errorText);
            }
        }

        private async Task AddNotification(UberEatWebhootNotification data, bool duplicate)
        {
            var JSONMetavalue = JsonConvert.SerializeObject(data.meta);
            UberEatNotification notificationEntity = new UberEatNotification
            {
                EventId = data.event_id,
                EventType = data.event_type,
                EventTime = data.event_time,
                Meta = JSONMetavalue,
                ResourceHref = data.resource_href
            };

            if (duplicate)
            {
                var uberEatSetting = _eposHubDbContext.UberEatSettings.Where(s => s.UberEatStoreId.Equals(data.meta.user_id)).FirstOrDefault();

                if (uberEatSetting != null && uberEatSetting.AddRepeatedEvent)
                {
                    // // To Record event callbacks of UberEat for futher analysis of uber data, in case of update, cancel order against same event id
                    notificationEntity.IsOrderProcess = true;
                    notificationEntity.Count = -1;
                    notificationEntity.Status = Uber_Constants.OrderStateDuplicate;
                }
                else
                {
                    return;
                }
            }
            else
            {
                // // New event
                notificationEntity.IsOrderProcess = false;
                notificationEntity.Count = 0;
                notificationEntity.Status = Uber_Constants.OrderStateCurrent;
            }

            notificationEntity.CreateDate = DateTime.UtcNow;
            var result = await SaveNotification(notificationEntity);
        }

        public async Task<long> SaveNotification(UberEatNotification notificationEntity)
        {
            var response = _eposHubDbContext.UberEatNotifications.Add(notificationEntity);
            var row = await _eposHubDbContext.SaveChangesAsync();
            if (row > 0)
                return notificationEntity.UberEatNotificationId;
            else
                return -1;
        }

        public async Task<bool> UpdateNotification(UberEatNotification notificationEntity)
        {
            var response = _eposHubDbContext.UberEatNotifications.Update(notificationEntity);
            var row = await _eposHubDbContext.SaveChangesAsync();
            if (row > 0)
                return true;
            else
                return false;
        }

        public async Task updateNotification(UberEatNotification notification)
        {
            // Updating  notification
            notification.ProcessDate = DateTime.UtcNow;
            notification.Count = notification.Count + 1;
            await UpdateNotification(notification);
        }

        public async Task<string> RegisterUser(Guid admincompanyud, HubRiseCallBackCustomer c, string eposhub_service)
        {
            OnlineCustomer onlineCustomer = new OnlineCustomer();
            onlineCustomer.Customer = new HubRiseCutomer
            {
                ClientUD = admincompanyud,
                FirstName = c.first_name,
                LastName = c.last_name,
                Telephone = c.phone,
                Username = c.email,
                Password = Encoding.UTF8.GetBytes(c.password)
            };
            if (string.IsNullOrEmpty(c.address_1) || string.IsNullOrEmpty(c.postal_code))
            {
                onlineCustomer.Address = null;
            }
            else
            {
                onlineCustomer.Address = new HubRiseCutomerAddress
                {
                    AddressLine1 = c.address_1,
                    AddressLine2 = c.address_2,
                    AddressName = "",
                    IsBillingAddress = true,
                    Postcode = c.postal_code,
                    TownOrCity = c.city
                };
            }

            var response = await ServicePost("RegisterCustomer", onlineCustomer, eposhub_service);
            if (response.ToString() == "-1" || response.ToString().Equals("ERROR"))
                return JsonConvert.SerializeObject(new ResponseRegisterUser { AddressID = -1, CustomerID = -1 });
            return response;
        }

        private async Task<string> ServicePost(string method, object parameter, string eposhub_service)
        {
            try
            {
                string requestUrl = eposhub_service;
                var result = await EposHubHelper.SendCall(requestUrl, method, parameter, "");

                if (!string.IsNullOrWhiteSpace(result))
                    return result;

                return "-1";
            }
            catch (Exception e)
            {
                return "-1";
            }
        }

        private SPosItem GetPosItemforUberItem(UberOrderItem orderItem, string currency_code)
        {
            if (orderItem.external_data.Contains(","))
            {
                SPosItem sPosItem = new SPosItem
                {
                    pos_item_id = Convert.ToInt32(orderItem.external_data.Split(",")[0]),
                    unit_price = (orderItem.price == null) ? 0 : (int)(Convert.ToDecimal(orderItem.price.Replace(" " + currency_code, string.Empty))),
                    pos_item_name = orderItem.external_data.Split(",")[1],
                    quantity = orderItem.quantity,
                    modifiers = new List<SPosModifier>()
                };
                return sPosItem;
            }
            else
            {
                SPosItem sPosItem = new SPosItem
                {
                    pos_item_id = Convert.ToInt32(orderItem.external_data),
                    unit_price = (orderItem.price == null) ? 0 : (int)(Convert.ToDecimal(orderItem.price.Replace(" " + currency_code, string.Empty))),
                    quantity = orderItem.quantity,
                    modifiers = new List<SPosModifier>()
                };
                return sPosItem;
            }
        }

        private SPosModifier GetSPosModifierforUberOption(UberSelectedOption orderOption, string currency_code)
        {
            if (!string.IsNullOrEmpty(orderOption.external_data))
            {
                if (orderOption.external_data.Contains(","))
                {
                    SPosModifier posModifier = new SPosModifier
                    {
                        pos_item_id = Convert.ToInt32(orderOption.external_data.Split(",")[0]),
                        unit_price = (orderOption.price == null) ? 0 : (int)(Convert.ToDecimal(orderOption.price.Replace(" " + currency_code, string.Empty))),
                        mod_name = orderOption.external_data.Split(",")[1]
                    };
                    return posModifier;
                }
                else
                {
                    SPosModifier posModifier = new SPosModifier
                    {
                        pos_item_id = Convert.ToInt32(orderOption.external_data),
                        unit_price = (orderOption.price == null) ? 0 : (int)(Convert.ToDecimal(orderOption.price.Replace(" " + currency_code, string.Empty)))
                    };
                    return posModifier;
                }
            }
            return new SPosModifier();
        }

        //QA
        public async void ProcessOrdersTest(UberEatOrder uberEatOrder)
        {
            try
            {
                UberEatNotification notificationEntity = new UberEatNotification();
                var res = await SaveNotification(notificationEntity);

                if (!isProcessingOrders)
                {
                    isProcessingOrders = true;
                    string hubriseReason = null;
                    long hubRiseOrderID = 0;
                    try
                    {
                        var metaData = JsonConvert.DeserializeObject<OrderMeta>(notificationEntity.Meta);
                        var uberEatSetting = _eposHubDbContext.UberEatSettings.Where(s => s.UberEatStoreId.Equals(metaData.user_id)).FirstOrDefault();

                        if (uberEatSetting != null && uberEatSetting.ProcessOrder)
                        {
                            string result = await HubRiseProcessOrder(uberEatOrder, uberEatSetting);
                            result = result.Replace("\"", "").Replace("\\", "").Replace(@"""\", "");
                            hubRiseOrderID = (long)Convert.ToDouble(result);
                        }
                    }
                    catch (System.Exception err)
                    {
                        hubriseReason = err.Message;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                isProcessingOrders = false;
            }
        }


    }

    public class ResponseRegisterUser
    {
        public int CustomerID { get; set; }
        public int AddressID { get; set; }
    }

}
