﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Common.Helpers;
using HubRise.Models;
using UberEatClient.Models;
using Microsoft.Extensions.Options;

namespace UberEatClient.Services
{
    public class TokensService
    {

        private static Dictionary<Guid, ClientAccessToken> ClientAccessToken = new Dictionary<Guid, ClientAccessToken>();

        private readonly ILogger _Logger;
        private readonly Common.EposHubDb.EposHubDbContext _eposHubDbContext;

        private readonly Settings _settings;

        public TokensService(Common.EposHubDb.EposHubDbContext eposHubDb, ILogger<TokensService> logger, IOptions<Settings> settings)
        {
            _Logger = logger;
            _eposHubDbContext = eposHubDb;
            _settings = settings.Value;
        }

        public string GetTokenOrder(Common.EposHubDb.UberEatSetting uberEatSetting, bool force = false)
        {
            string strToken = "";
            DateTime expireDateTime = DateTime.Now.AddDays(29);
            string tokenType = Uber_Constants.TokenScpOrdVal;
            var token = ClientAccessToken.FirstOrDefault(wh => wh.Key == uberEatSetting.ClientUD);

            if (force || token.Key == null || token.Value == null
                || (token.Value != null && string.IsNullOrEmpty(token.Value.OrderToken)))
            {
                UberAccessToken tokenAdd = UberEatHelper.GetToken(uberEatSetting.AppClientId, uberEatSetting.AppClientSecret, tokenType).Result;
                expireDateTime = DateTime.Now.AddSeconds(tokenAdd.expires_in);
                strToken = tokenAdd.access_token;
                force = true; //  to add/update token below
            }
            else
            {
                if (token.Value.DateOrderTokenExpire.AddDays(-1) < DateTime.Now) // refresh day before expiry
                {
                    UberAccessToken tokenEdit = UberEatHelper.GetToken(uberEatSetting.AppClientId, uberEatSetting.AppClientSecret, tokenType).Result;
                    expireDateTime = DateTime.Now.AddSeconds(tokenEdit.expires_in);
                    strToken = tokenEdit.access_token;
                    force = true; //  to add/update token below
                }
            }

            if (token.Key == null || token.Value == null)
            {
                ClientAccessToken.Add(uberEatSetting.ClientUD, new Services.ClientAccessToken()
                {
                    DateOrderTokenExpire = expireDateTime,
                    DateOrderTokenIssue = DateTime.Now,
                    OrderToken = strToken
                });

            }
            else
            {
                if (force)
                {
                    token.Value.DateOrderTokenExpire = expireDateTime;
                    token.Value.DateOrderTokenIssue = DateTime.Now;
                    token.Value.OrderToken = strToken;
                }
                else
                    strToken = token.Value.OrderToken;
            }

            return strToken;
        }

        public string GetTokenStore(Common.EposHubDb.UberEatSetting uberEatSetting, bool force = false)
        {
            string strToken = "";
            DateTime expireDateTime = DateTime.Now.AddDays(29);
            string tokenType = Uber_Constants.TokenScpStorVal;
            var token = ClientAccessToken.FirstOrDefault(wh => wh.Key == uberEatSetting.ClientUD);

            if (force || token.Key == null || token.Value == null ||
                (token.Value != null && string.IsNullOrEmpty(token.Value.StoreToken)))
            {
                UberAccessToken tokenAdd = UberEatHelper.GetToken(uberEatSetting.AppClientId, uberEatSetting.AppClientSecret, tokenType).Result;
                expireDateTime = DateTime.Now.AddSeconds(tokenAdd.expires_in);
                strToken = tokenAdd.access_token;
                force = true; //  to add/update token below
            }
            else
            {
                if (token.Value.DateStoreTokenExpire.AddDays(-1) < DateTime.Now) // refresh day before expiry
                {
                    UberAccessToken tokenEdit = UberEatHelper.GetToken(uberEatSetting.AppClientId, uberEatSetting.AppClientSecret, tokenType).Result;
                    expireDateTime = DateTime.Now.AddSeconds(tokenEdit.expires_in);
                    strToken = tokenEdit.access_token;
                    force = true; //  to add/update token below
                }
            }

            if (token.Key == null || token.Value == null)
            {
                ClientAccessToken.Add(uberEatSetting.ClientUD, new Services.ClientAccessToken()
                {
                    DateStoreTokenExpire = expireDateTime,
                    DateStoreTokenIssue = DateTime.Now,
                    StoreToken = strToken
                });

            }
            else
            {
                if (force)
                {
                    token.Value.DateStoreTokenExpire = expireDateTime;
                    token.Value.DateStoreTokenIssue = DateTime.Now;
                    token.Value.StoreToken = strToken;
                }
                else
                    strToken = token.Value.StoreToken;
            }

            return strToken;
        }

    }

    public class ClientAccessToken
    {
        public DateTime DateOrderTokenExpire;
        public DateTime DateStoreTokenExpire;
        public DateTime DateOrderTokenIssue;
        public DateTime DateStoreTokenIssue;
        public string OrderToken = "";
        public string StoreToken = "";
    }
}
