﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Common.Helpers;
using HubRise.Models;
using UberEatClient.Models;

namespace UberEatClient.Services
{
    internal class MenuSyncService : TimedHostedService
    {
        private readonly IServiceProvider _services;
        private readonly string ImageFolderUrlSetting = "wwwroot\\images\\";
        private readonly string HUBRISE_BASE_URL = "https://api.hubrise.com/v1";

        public MenuSyncService(IServiceProvider services, ILogger<MenuSyncService> logger)
            : base(logger)
        {
            _services = services;
        }

        bool onlyOneAtaTime = true;
        protected async override void DoWork(object state)
        {
            if (onlyOneAtaTime)
            {
                onlyOneAtaTime = false;
                try
                {
                    using (var scope = _services.CreateScope())
                    {
                        var eposHubDb = scope.ServiceProvider.GetRequiredService<Common.EposHubDb.EposHubDbContext>();
                        var eposAdminDb = scope.ServiceProvider.GetRequiredService<Common.EposAdmin.EposAdminContext>();
                        var tokenService = scope.ServiceProvider.GetRequiredService<TokensService>();
                        // // List<Common.EposHubDb.Station> changedItems = await eposHubDb.Stations.Where(w => w.IsMenuChanged && !string.IsNullOrWhiteSpace(w.UberEatStoreID)).ToListAsync();
                        List<Common.EposHubDb.Station> changedItems = await (from station in eposHubDb.Stations
                                                                             join uberSetting in eposHubDb.UberEatSettings
                                                                             on station.ClientUd equals uberSetting.ClientUD
                                                                             where station.IsMenuChanged
                                                                             select station
                                                                             ).ToListAsync();

                        changedItems.ForEach(r => UploadMenu(r, eposHubDb, eposAdminDb, tokenService));

                    }
                }
                catch (Exception e)
                {
                    Logger.LogError($"Error in Uber Eat Sync Service {e.Message}");
                }
                finally
                {
                    onlyOneAtaTime = true;
                }
            }
        }

        private async Task<string> GetCompanyName(string clientID, Common.EposAdmin.EposAdminContext eposAdminDb)
        {
            string cmpName = await (from cmp in eposAdminDb.Companies
                                    join client in eposAdminDb.Clients
                                    on cmp.CompanyId equals client.CompanyId
                                    where client.ClientId.ToString().Equals(clientID)
                                    select cmp.Name).FirstOrDefaultAsync();

            return cmpName;

        }
        public void UploadMenu(Common.EposHubDb.Station hubStation, Common.EposHubDb.EposHubDbContext eposHubDb, Common.EposAdmin.EposAdminContext eposAdminDb,
            TokensService tokensService)
        {
            var uberEatSetting = eposHubDb.UberEatSettings.Where(s => s.ClientUD.Equals(hubStation.ClientUd)).FirstOrDefault();
            if (uberEatSetting != null && uberEatSetting.ProcessMenu)
            {
                string SPosMenuJson = "", companyName = "", HubRiseAccessToken = "", HubRiseMennuID = "";

                companyName = GetCompanyName(hubStation.ClientUd.ToString(), eposAdminDb).Result;
                if (string.IsNullOrEmpty(companyName)) return;
                HubRiseAccessToken = hubStation.HubRiseAccessToken;
                //if (deliveryType == "delivery")
                //{
                SPosMenuJson = hubStation.MenuDelivery;
                HubRiseMennuID = hubStation.HubRiseMenuDeliveryId;
                //}
                //else if (deliveryType == "collection")
                //{
                //    SPosMenuJson = hubStation.MenuCollection;
                //    HubRiseMennuID = hubStation.HubRiseMenuCollectionId;
                //}


                if (string.IsNullOrEmpty(SPosMenuJson)) return;
                HubRise.Models.BranchMenu branchMenu = JsonConvert.DeserializeObject<HubRise.Models.BranchMenu>(JsonConvert.DeserializeObject(SPosMenuJson).ToString());
                if (branchMenu == null) return;

                HubRise.Models.BranchSettings branchSettings = null;
                if (!string.IsNullOrWhiteSpace(hubStation.BranchSettings))
                    branchSettings = JsonConvert.DeserializeObject<HubRise.Models.BranchSettings>(hubStation.BranchSettings);


                var imagesDataTask = UploadImagesToHubRise(branchMenu, companyName, HubRiseMennuID, HubRiseAccessToken);
                var imagesData = imagesDataTask.Result;

                UberEatMenu uberEatMenu = ConvertToUberEatMenu(branchMenu, branchSettings, uberEatSetting, imagesData);
                if (uberEatMenu == null) return;
                var uberEatMenuJson = Newtonsoft.Json.JsonConvert.SerializeObject(uberEatMenu);

                // // upload uberEatMenuJson on store 
                bool result = UberEatHelper.PutMenu(uberEatMenu, tokensService.GetTokenStore(uberEatSetting), uberEatSetting.UberEatStoreId).Result;
                hubStation.IsMenuChanged = false;
                eposHubDb.Entry(hubStation).Property(p => p.IsMenuChanged).IsModified = true;
                eposHubDb.SaveChanges();
            }
        }


        private async Task<List<ImageData>> UploadImagesToHubRise(BranchMenu menu, string CompanyName, string catalogId, string token)
        {
            string drive = Environment.SystemDirectory.Substring(0, 2);

            DirectoryInfo imagesDir = new DirectoryInfo(drive + @"\inetpub\" + ImageFolderUrlSetting + CompanyName.ToUpper());

            try
            {
                if (imagesDir.Exists)
                {
                    var localImagesData = GetLocalImagesData(menu, imagesDir);

                    var hubRiseImagesData = await GetHubRiseImagesData(catalogId, token);

                    if (localImagesData == null || hubRiseImagesData == null) return null;

                    foreach (var iData in localImagesData)
                    {
                        var hrIData = hubRiseImagesData.Find(li => li.md5 == iData.md5);

                        if (hrIData == null)
                        {
                            Dictionary<string, string> headers = new Dictionary<string, string>();
                            headers.Add("Content-Type", iData.type);
                            headers.Add("X-Access-Token", token);

                            var receivedImageData = await ApiClient.CallApi<ImageData>(HUBRISE_BASE_URL, "catalogs/" + catalogId + "/images", iData.binary, headers, RestApiCallType.POST, true);

                            if (receivedImageData != null)
                            {
                                iData.Id = receivedImageData.Id; // assign image an Id
                            }
                        }
                        else
                        {
                            iData.Id = hrIData.Id;  // update image Id
                        }

                        iData.binary = null; // we dont need binary aymore
                    }
                    return localImagesData;
                }
                return null;
            }
            catch (Exception e)
            {
                Logger.LogError("****UploadImagesToHubRise-Exception" + e.Message);
                return null;
            }
        }

        private async Task<List<ImageData>> GetHubRiseImagesData(string catalogId, string token)
        {
            try
            {
                Dictionary<string, string> headers = new Dictionary<string, string>();
                headers.Add("Content-Type", "application/json");
                headers.Add("X-Access-Token", token);

                return await ApiClient.CallApi<List<ImageData>>(HUBRISE_BASE_URL, "/catalogs/" + catalogId + "/images", null, headers, RestApiCallType.GET, false);
            }
            catch { return null; }
        }

        private List<ImageData> GetLocalImagesData(BranchMenu menu, DirectoryInfo imagesDir)
        {
            List<ImageData> imagesData = new List<ImageData>();

            // Menu images
            foreach (var category in menu.Categories)
            {
                foreach (var product in category.Products)
                {
                    foreach (var imageName in product.ImageUrls)
                    {
                        if (!string.IsNullOrEmpty(imageName))
                        {
                            var fileInfo = imagesDir.GetFiles(imageName);
                            if (fileInfo.Length > 0) // found the file 
                            {
                                var iData = GetImageData(fileInfo);

                                if (iData == null) continue;
                                if (string.IsNullOrEmpty(iData.md5)) continue;

                                imagesData.Add(iData);
                            }
                        }
                    }
                }
            }

            // Deal images
            foreach (var deal in menu.Deals)
            {
                if (!string.IsNullOrEmpty(deal.ImageUrl))
                {
                    var fileInfo = imagesDir.GetFiles(deal.ImageUrl);
                    if (fileInfo.Length > 0) // found the file 
                    {
                        var iData = GetImageData(fileInfo);

                        if (iData == null) continue;
                        if (string.IsNullOrEmpty(iData.md5)) continue;

                        imagesData.Add(iData);
                    }
                }
            }

            return imagesData;
        }

        private static ImageData GetImageData(FileInfo[] fileInfo)
        {
            if (fileInfo == null) return null;
            if (fileInfo.Length <= 0) return null;

            ImageData iData = new ImageData
            {
                name = fileInfo[0].Name
            };

            switch (fileInfo[0].Extension)
            {
                case ".jpg":
                    iData.type = "image/jpeg";
                    break;
                case ".jpeg":
                    iData.type = "image/jpeg";
                    break;
                case ".jpe":
                    iData.type = "image/jpeg";
                    break;
                case ".gif":
                    iData.type = "image/gif";
                    break;
                case ".png":
                    iData.type = "image/png";
                    break;
                case ".tiff":
                    iData.type = "image/tiff";
                    break;
                default:
                    return null; // probably not an image, ignore this file.
            }

            string md5Hash = GetMd5Hash(fileInfo[0].FullName, ref iData); // updates iData.binary as well

            if (string.IsNullOrEmpty(md5Hash))
                return null; // true, only if file is corrupt

            iData.md5 = md5Hash;

            return iData;
        }

        private static string GetMd5Hash(string filePath, ref ImageData iData)
        {
            try
            {
                using (var md5 = MD5.Create())
                {
                    StringBuilder sb = new StringBuilder();

                    iData.binary = File.ReadAllBytes(filePath);
                    if (iData.binary == null) return null;

                    // serialise and encoding changes md5hash, hence using the same as Api call for parity
                    //var payload = JsonConvert.SerializeObject(iData.binary);
                    //byte[] hashBytes = md5.ComputeHash(Encoding.Default.GetBytes(payload));

                    byte[] hashBytes = md5.ComputeHash(iData.binary);

                    if (hashBytes == null) return null;

                    foreach (byte bt in hashBytes)
                    {
                        sb.Append(bt.ToString("x2"));
                    }

                    return sb.ToString();
                }
            }
            catch { return null; }
        }

        private UberEatMenu ConvertToUberEatMenu(HubRise.Models.BranchMenu branchMenu, HubRise.Models.BranchSettings branchSettings, Common.EposHubDb.UberEatSetting ubereatSetting, List<ImageData> imagesData)
        {
            if (branchMenu == null) return null;
            string CurrNameWithSpace = " " + ubereatSetting.CurrencyShortName;

            UberEatMenu uberEatMenu = new UberEatMenu()
            {
                sections = new List<Section>()
            };

            List<ServiceAvailability> uberMenuAvailability = new List<ServiceAvailability>();
            if (branchSettings != null && branchSettings.DeliveryTypes != null && branchSettings.DeliveryTypes.Count > 0)
            {
                var deliveryBrnList = branchSettings.DeliveryTypes.FindAll(f => f.IsPickup == false);
                if (deliveryBrnList != null)
                {
                    foreach (var deliveryBrn in deliveryBrnList)
                    {
                        foreach (var dlvSlot in deliveryBrn.DeliverySlots)
                        {
                            List<TimePeriod> timePeriods = new List<TimePeriod>();
                            //timePeriods.Add(new TimePeriod()
                            //{
                            //    end_time = dlvSlot.EndTime.ToShortTimeString(),
                            //    start_time = dlvSlot.StartTime.ToShortTimeString()
                            //});
                            // // BZK todo, for testing , else open above line and comment below one
                            timePeriods.Add(new TimePeriod()
                            {
                                end_time = "23:59",
                                start_time = "00:00"
                            });

                            uberMenuAvailability.Add(new ServiceAvailability()
                            {
                                day_of_week = nameof(dlvSlot.Monday).ToLower(),
                                enabled = dlvSlot.Monday,
                                time_periods = timePeriods
                            });
                            uberMenuAvailability.Add(new ServiceAvailability()
                            {
                                day_of_week = nameof(dlvSlot.Tuesday).ToLower(),
                                enabled = dlvSlot.Tuesday,
                                time_periods = timePeriods
                            });
                            uberMenuAvailability.Add(new ServiceAvailability()
                            {
                                day_of_week = nameof(dlvSlot.Wednesday).ToLower(),
                                enabled = dlvSlot.Wednesday,
                                time_periods = timePeriods
                            });
                            uberMenuAvailability.Add(new ServiceAvailability()
                            {
                                day_of_week = nameof(dlvSlot.Thursday).ToLower(),
                                enabled = dlvSlot.Thursday,
                                time_periods = timePeriods
                            });
                            uberMenuAvailability.Add(new ServiceAvailability()
                            {
                                day_of_week = nameof(dlvSlot.Friday).ToLower(),
                                enabled = dlvSlot.Friday,
                                time_periods = timePeriods
                            });
                            uberMenuAvailability.Add(new ServiceAvailability()
                            {
                                day_of_week = nameof(dlvSlot.Saturday).ToLower(),
                                enabled = dlvSlot.Saturday,
                                time_periods = timePeriods
                            });
                            uberMenuAvailability.Add(new ServiceAvailability()
                            {
                                day_of_week = nameof(dlvSlot.Sunday).ToLower(),
                                enabled = dlvSlot.Sunday,
                                time_periods = timePeriods
                            });

                        }

                    }
                }
            }

            List<Subsection> uberSubSection = new List<Subsection>();

            if (branchMenu != null && branchMenu.Categories != null && branchMenu.Categories.Count > 0)
            {
                foreach (var category in branchMenu.Categories)
                {
                    List<UberItem> uberItems = new List<UberItem>();
                    foreach (var product in category.Products)
                    {
                        if (product.Sizes == null) continue;

                        List<string> imageIds = new List<string>();
                        string firstImg = "";
                        if (product.ImageUrls == null)
                        {
                            product.ImageUrls = new List<string>();
                        }
                        firstImg = GetImage(imagesData, product.ImageUrls, imageIds);

                        uberItems = new List<UberItem>();
                        foreach (var sku in product.Sizes)
                        {

                            List<Customization> customizations = new List<Customization>();
                            foreach (var option in product.Modifiers)
                            {
                                HubRise.Models.WebListItem mod = null;

                                List<CustomizationOption> customization_options = new List<CustomizationOption>();
                                Customization customization = new Customization()
                                {
                                    title = option.Header
                                };

                                foreach (var optionSize in option.Sizes)
                                {
                                    if (optionSize.ItemID == sku.ItemID)
                                    {
                                        mod = optionSize;
                                        break;
                                    }
                                    //CustomizationOption customizationOption = new CustomizationOption()
                                    //{
                                    //    external_id = optionSize.VariantID.ToString(),
                                    //    price = (double)optionSize.Price,
                                    //    title = optionSize.Text
                                    //};
                                    //customization.customization_options.Add(customizationOption);
                                }

                                if (option.Header.ToLower().Contains("ingredients"))
                                    option.Header = "ingredients";

                                CustomizationOption uberCustomizOpt = new CustomizationOption
                                {
                                    title = option.Text,
                                    price = (double)((mod == null) ? option.Price : mod.Price),
                                    external_id = (mod == null) ? option.ItemID.ToString() : mod.VariantID.ToString()
                                };

                                switch (option.Header.ToLower())
                                {
                                    case "ingredients":
                                        uberCustomizOpt.price = 0;

                                        break;
                                    case "options":

                                        // //     tags = new List<string> { option.Header.ToString() 

                                        break;
                                    default:
                                        if (option.ForceNumber != null && option.ForceNumber.HasValue)
                                        {
                                            customization.min_permitted = option.ForceNumber.Value;
                                            customization.max_permitted = option.ForceNumber.Value;
                                        }

                                        break;
                                }

                                customization.customization_options.Add(uberCustomizOpt);


                                customization.customization_options = customization_options;
                                customizations.Add(customization);
                            }


                            uberItems.Add(new UberItem
                            {
                                title = product.Name,
                                price = (double)product.SinglePrice,
                                currency_code = ubereatSetting.CurrencyShortName,
                                external_id = sku.VariantID.ToString(),
                                image_url = firstImg,
                                //external_id = product.ProductID.ToString() + sku.VariantID.ToString(),
                                item_description = product.Description,
                                is_alcohol = false,
                                customizations = customizations


                            });
                        }

                    }
                    uberSubSection.Add(new Subsection()
                    {
                        items = uberItems,
                        title = category.Name,
                        subsection_id = Guid.NewGuid().ToString().ToLower()
                    });
                }

                uberEatMenu.sections.Add(
                        new Section()
                        {
                            subtitle = "Open 24/7!",
                            title = "Main Course",
                            service_availability = uberMenuAvailability,
                            subsections = uberSubSection

                        });
            }

            // Add Deals
            List<Subsection> dealsSubSection = new List<Subsection>();
            if (branchMenu != null && branchMenu.Deals != null && branchMenu.Deals.Count > 0)
            {
                foreach (var deal in branchMenu.Deals)
                {
                    var hrDeal = new Subsection
                    {
                        title = deal.Name,
                        subsection_id = deal.ID.ToString(),
                        items = new List<UberItem>()
                    };

                    List<string> imageIds = new List<string>();
                    string firstImg = "";
                    List<string> ImageUrls = new List<string>();
                    ImageUrls.Add(deal.ImageUrl);
                    firstImg = GetImage(imagesData, ImageUrls, imageIds);

                    foreach (var pGroup in deal.ProductGroups)
                    {
                        var line = new UberItem
                        {
                            title = string.IsNullOrWhiteSpace(pGroup.Weblabel) ? pGroup.Name : pGroup.Weblabel,
                            price = (double)deal.Price,
                            image_url = firstImg,
                            currency_code = ubereatSetting.CurrencyShortName,
                        };
                        foreach (var set in pGroup.ProductVariants)
                        {
                            // Added code to stop adding line.skus which deos not exists in category.Products  - BZK 18/05/08 
                            bool stopAddSkus = false;
                            foreach (var category in branchMenu.Categories)
                            {
                                foreach (var product in category.Products)
                                {
                                    if (product.Sizes.Find(f => f.VariantID.Equals(set.VariantID)) != null)
                                    {
                                        line.external_id = set.VariantID.ToString();

                                        stopAddSkus = true;
                                        break;
                                    }
                                }
                                if (stopAddSkus)
                                {
                                    break;
                                }
                            }
                        }

                        //for (int i = 0; i < pGroup.Number; i++)
                        //{
                        //    if (pGroup.WeblabelOrder == 1 && i == 0)
                        //    {                          
                        //        line.pricing_effect = "fixed_price";
                        //        line.pricing_value = deal.Price.ToString("F") + CurrNameWithSpace;
                        //    }
                        //    else
                        //    {
                        //        line.pricing_effect = "percentage_off";
                        //        line.pricing_value = "100";
                        //    }
                        //}

                        hrDeal.items.Add(line);
                    }
                    dealsSubSection.Add(hrDeal);
                }
                uberEatMenu.sections.Add(
                    new Section()
                    {
                        subtitle = "Open 24/7!",
                        title = "Deals",
                        service_availability = uberMenuAvailability,
                        subsections = dealsSubSection

                    });
            }

            return uberEatMenu;
        }

        private static string GetImage(List<ImageData> imagesData, List<string> ImageUrls, List<string> imageIds)
        {
            string firstImg = "";
            if (imagesData != null)
            {
                foreach (var imageName in ImageUrls)
                {
                    var imageData = imagesData?.Find(id => id.name == imageName);
                    if (imageData != null)
                    {
                        imageIds.Add(imageData.Id);
                    }
                }
                if (imageIds.Count > 0)
                    firstImg = imageIds[0];
            }
            return firstImg;
        }

        private UberEatMenu ConvertToUberEatMenuForCategories(HubRise.Models.BranchMenu branchMenu, Common.EposHubDb.UberEatSetting uberEatSettings, HubRise.Models.BranchSettings branchSettings)
        {
            if (branchMenu == null) return null;
            string CurrNameWithSpace = " " + uberEatSettings.CurrencyShortName;

            UberEatMenu uberEatMenu = new UberEatMenu()
            {
                sections = new List<Section>()
            };

            List<ServiceAvailability> uberMenuAvailability = new List<ServiceAvailability>();
            List<UberItem> uberItems = new List<UberItem>();
            List<Subsection> uberSubSection = new List<Subsection>();

            foreach (var category in branchMenu.Categories)
            {
                uberSubSection = new List<Subsection>();
                foreach (var product in category.Products)
                {
                    uberItems = new List<UberItem>();
                    foreach (var sku in product.Sizes)
                    {
                        uberItems.Add(new UberItem
                        {
                            title = product.Name,
                            price = (double)product.SinglePrice,
                            currency_code = uberEatSettings.CurrencyShortName,
                            external_id = product.ProductID.ToString() + sku.VariantID.ToString(),
                            item_description = product.Description,
                            is_alcohol = false

                        });
                    }
                    uberSubSection.Add(new Subsection()
                    {
                        items = uberItems,
                        title = product.Name
                    });
                }

                uberEatMenu.sections.Add(
                new Section()
                {
                    title = category.Name,
                    service_availability = uberMenuAvailability,
                    subsections = uberSubSection

                });
            }

            return uberEatMenu;
        }


    }
}
