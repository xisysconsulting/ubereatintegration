﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UberEatClient.Models;
using UberEatClient.Services;

namespace UberEatIntegration.Controllers
{
    [Route("api/ubereat")]
    [ApiController]
    public class UberEatController : ControllerBase
    {
        private readonly Common.EposHubDb.EposHubDbContext _eposHubDbContext;
        private readonly OrderService _orderService;

        public UberEatController(Common.EposHubDb.EposHubDbContext eposHubDb, OrderService orderService)
        {
            _eposHubDbContext = eposHubDb;
            _orderService = orderService;
        }

        [HttpGet("oaut")]
        public async Task<IActionResult> Oauth()
        {
            try
            {
                return Ok(Request.QueryString.Value);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("redirecturi")]
        public async Task<IActionResult> RedirectUri()
        {
            try
            {
                return Ok(Request.QueryString.Value);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("privacypolicyuri")]
        public async Task<IActionResult> PrivacyPolicyUri()
        {
            try
            {
                return Ok(true);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("originuri")]
        public async Task<IActionResult> OriginUri([FromBody] dynamic data)
        {
            try
            {
                return Ok(true);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("surge")]
        public async Task<IActionResult> SurgeConfirmationUri([FromBody] dynamic data)
        {
            try
            {
                return Ok(true);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("webhook")]
        [AllowAnonymous]
        public async Task<IActionResult> WebhookURL([FromBody] Object data)
        {
            try
            {
                string strData = Newtonsoft.Json.JsonConvert.SerializeObject(data);
                UberEatWebhootNotification webhookData = Newtonsoft.Json.JsonConvert.DeserializeObject<UberEatWebhootNotification>(strData);

                await _orderService.ProcessNotification(webhookData);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("storestatus/{storeid}")]
        public async Task<IActionResult> GetStoreStatus([FromRoute] string storeid)
        {
            try
            {
                return Ok(await _orderService.GetStoreStatus(storeid));
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("store/{storeid}")]
        public async Task<IActionResult> GetStoreDetail([FromRoute] string storeid)
        {
            try
            {
                return Ok(await _orderService.GetStoreDetails(storeid));
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPatch("updatestorestatus/{storeid}/{on}")]
        public async Task<IActionResult> PatchStoreStatus([FromRoute] string storeid, [FromRoute] bool on)
        {
            try
            {
                await _orderService.SetStoreStatus(storeid, on);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("test-order")]
        public async Task<IActionResult> OrderTest([FromBody] UberEatOrder data)
        {
            try
            {
                _orderService.ProcessOrdersTest(data);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("test-process")]
        public async Task<IActionResult> ProcessTest()
        {
            try
            {
                await _orderService.ProcessOrders();
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}