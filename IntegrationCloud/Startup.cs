﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using UberEatClient.Models;

namespace UberEatIntegration
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDbContext<Common.EposHubDb.EposHubDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("EPOSHubDb")));

            services.AddDbContext<Common.EposAdmin.EposAdminContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("EPOSAdminDb")));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "UberEat Integration API", Version = "v1" });
            });

            services.AddHostedService<UberEatClient.Services.MenuSyncService>();
            services.AddScoped<UberEatClient.Services.TokensService>();
            services.AddScoped<UberEatClient.Services.OrderService>();
            services.Configure<UberEatCustomer>(Configuration.GetSection("UberEatCustomer"));
            services.Configure<Settings>(Configuration.GetSection("Settings"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "UberEat Integration API V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseCors(c => c.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin().AllowCredentials());

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
