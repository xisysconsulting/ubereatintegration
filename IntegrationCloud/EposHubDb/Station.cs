﻿using System;

namespace Common.EposHubDb
{
    public class Station
    {
        public Guid ClientUd { get; set; }
        public int ProviderId { get; set; }
        public string ProviderLocationId { get; set; }
        public string LocationStationName { get; set; }
        public string MenuDelivery { get; set; }
        public string BranchSettings { get; set; }
        public string MenuCollection { get; set; }
        public string HubRiseAccessToken { get; set; }
        public bool IsMenuChanged { get; set; }
        public string HubRiseMenuDeliveryId { get; set; }
        public string HubRiseMenuCollectionId { get; set; }
        public string SSKServerSetting { get; set; }

    }
}