﻿using System;

namespace Common.EposHubDb
{
    public class Order
    {
        public long OrderId { get; set; }
        public int ProviderId { get; set; }
        public string ProviderLocationId { get; set; }
        public string ProviderOrderId { get; set; }
        public int CustomerId { get; set; }
        public string OrderName { get; set; }
        public DateTime OrderTime { get; set; }
        public DateTime PickUpTime { get; set; }
        public string OrderJSON { get; set; }
    }
}