﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using UberEatClient.Models;

namespace Common.EposHubDb
{
    public partial class EposHubDbContext : DbContext
    {
        public EposHubDbContext()
        {
        }

        public EposHubDbContext(DbContextOptions<EposHubDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<UberEatSetting> UberEatSettings { get; set; }

        public virtual DbSet<Providers> Providers { get; set; }

        public virtual DbSet<Station> Stations { get; set; }

        public virtual DbSet<HubRiseCutomer> Customers { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<UberEatNotification> UberEatNotifications { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                //  optionsBuilder.UseSqlServer("Data Source=(localdb)\\ProjectsV13;Initial Catalog=EposHubDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {


            modelBuilder.Entity<Providers>(entity =>
            {
                entity.HasKey(e => e.ProviderId);

                entity.Property(e => e.ProviderId).HasColumnName("ProviderID");

                //entity.Property(e => e.ApiPassword).HasMaxLength(100);

                //entity.Property(e => e.ApiUrl).HasMaxLength(1000);

                //entity.Property(e => e.ApiUsername).HasMaxLength(100);

                //entity.Property(e => e.CustomSignatureKey).HasMaxLength(100);

                //entity.Property(e => e.CustomerSequenceIdkey)
                //    .HasColumnName("CustomerSequenceIDKey")
                //    .HasMaxLength(100);

                entity.Property(e => e.ProviderName)
                    .IsRequired()
                    .HasMaxLength(100);

                //entity.Property(e => e.SharedPassword)
                //    .IsRequired()
                //    .HasMaxLength(100);
            });

            modelBuilder.Entity<Station>(entity =>
            {
                entity.HasKey(e => new { e.ClientUd, e.ProviderId });

                entity.Property(e => e.ClientUd).HasColumnName("ClientUD");

                entity.Property(e => e.ProviderId).HasColumnName("ProviderID");

                entity.Property(e => e.HubRiseAccessToken).HasMaxLength(200);

                entity.Property(e => e.HubRiseMenuCollectionId)
                    .HasColumnName("HubRiseMenuCollectionID")
                    .HasMaxLength(50);

                entity.Property(e => e.HubRiseMenuDeliveryId)
                    .HasColumnName("HubRiseMenuDeliveryID")
                    .HasMaxLength(50);

                entity.Property(e => e.LocationStationName).HasMaxLength(50);

                entity.Property(e => e.ProviderLocationId)
                    .IsRequired()
                    .HasColumnName("ProviderLocationID")
                    .HasMaxLength(200);

                //entity.HasOne(d => d.Provider)
                //    .WithMany(p => p.Stations)
                //    .HasForeignKey(d => d.ProviderId)
                //    .OnDelete(DeleteBehavior.ClientSetNull)
                //    .HasConstraintName("FK_Stations_Providers");
            });

            modelBuilder.Entity<HubRiseCutomer>(entity =>
            {
                entity.HasKey(e => new { e.CustomerID });
                //entity.Property(e => e.CustomerID).HasColumnName("CustomerID");
                //entity.Property(e => e.ClientUD).HasColumnName("ClientUD");
                //entity.Property(e => e.Username).HasColumnName("Username");
                //entity.Property(e => e.Password).HasColumnName("Password");
                //entity.Property(e => e.FirstName).HasColumnName("FirstName");
                //entity.Property(e => e.LastName).HasColumnName("LastName");
                //entity.Property(e => e.serializing).HasColumnName("CustomerID");
                //entity.Property(e => e.CustomerID).HasColumnName("CustomerID");

            });
            //
            modelBuilder.Entity<UberEatNotification>(entity =>
            {
                entity.HasKey(e => new { e.UberEatNotificationId });
            });

            modelBuilder.Entity<UberEatSetting>(entity =>
            {
                entity.HasKey(e => new { e.UberEatSettingId });

            });
        }
    }
}
