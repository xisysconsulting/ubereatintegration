﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.EposHubDb
{
    public partial class Providers
    {
        public Providers()
        {
            //Stations = new HashSet<Stations>();
        }

        public int ProviderId { get; set; }
        public string ProviderName { get; set; }
        //public string CustomSignatureKey { get; set; }
        //public string CustomerSequenceIdkey { get; set; }
        //public string SharedPassword { get; set; }
        //public string ApiUrl { get; set; }
        //public string ApiUsername { get; set; }
        //public string ApiPassword { get; set; }

        //public ICollection<Stations> Stations { get; set; }
    }
}
