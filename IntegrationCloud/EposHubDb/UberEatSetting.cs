﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.EposHubDb
{
    public class UberEatSetting
    {
        public long UberEatSettingId { get; set; }
        public Guid ClientUD { get; set; }
        public string UberEatStoreId { get; set; }
        public string CurrencyShortName { get; set; }
        public string EposhubService { get; set; }
        public string AppClientId { get; set; }
        public string AppClientSecret { get; set; }
        public bool SetPosDataOn { get; set; }
        public bool ProcessOrder { get; set; }
        public bool ProcessMenu { get; set; }
        public bool AddRepeatedEvent { get; set; }
    }
}
