﻿using System;

namespace Common.EposHubDb
{
    public class StationDto
    {
        public Guid ClientUd { get; set; }
        public int ProviderId { get; set; }
        public string ProviderLocationId { get; set; }
        public string LocationStationName { get; set; }

        //public bool IsSSK { get; set; }
        //public bool IsOnlineOrder { get; set; }
        //public bool IsLoyalty { get; set; } //IsLoyalty
        public string Product { get; set; }

    }

    public class EPosHubStationDto
    {
        public Guid ClientUd { get; set; }
        public int ProviderId { get; set; }
        public string ProviderLocationId { get; set; }
        public string LocationStationName { get; set; }
        public bool IsMenuChanged { get; set; }
    }
}