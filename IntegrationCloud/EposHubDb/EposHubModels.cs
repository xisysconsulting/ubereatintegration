﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.EposHubDb
{
    public class SPosModifier
    {
        public int pos_item_id { get; set; }
        public int unit_price { get; set; }
        public string mod_name { get; set; }
    }
    public class SPosItem
    {
        public string pos_item_name { get; set; }
        public int pos_item_id { get; set; }
        public int unit_price { get; set; }
        public int quantity { get; set; }
        public List<SPosModifier> modifiers { get; set; }
    }
    public class SPosOrder
    {
        public string order_id { get; set; }
        public string name { get; set; }
        public string order_type { get; set; }
        public string asap { get; set; }
        public string pickup_at { get; set; }
        public int total { get; set; }
        public string currency { get; set; }
        public string notes { get; set; }
        public List<SPosItem> items { get; set; }
        public string OrderJSON { get; set; }
        public bool? is_paid { get; set; }
        public bool? is_collection { get; set; }

        public bool? is_table { get; set; }
    }
    public class SPosPayment
    {
        public int PaymentType { get; set; }
        public decimal Amount { get; set; }
        public string AuthCode { get; set; }
        public DateTime PaymentTime { get; set; }
    }
    public class SPosOrderEvent
    {
        public string @event { get; set; }
        public string acknowledged_at { get; set; }
        public string location_id { get; set; }
        public SPosOrder order { get; set; }
        public OnlineCustomer customer { get; set; }
        public List<SPosPayment> payment_records { get; set; }
    }
    public class OnlineCustomer
    {
        public HubRiseCutomer Customer { get; set; }
        public HubRiseCutomerAddress Address { get; set; }
    }

    public class HubRiseCutomer
    {
        public int CustomerID { get; set; }
        public System.Guid ClientUD { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public byte[] Password { get; set; }
        public string LastName { get; set; }
        public string Telephone { get; set; }
        public string SavedCardToken { get; set; }

        public HubRiseCutomer()
        {
            ClientUD = Guid.Empty;
        }
    }

    public class HubRiseCutomerAddress
    {
        public int AddressID;
        public int CustomerID;
        public string AddressName;
        public string AddressLine1;
        public string AddressLine2;
        public string TownOrCity;
        public string Postcode;
        public bool IsBillingAddress;
        //public bool serializing;
    }

    public class Credentials
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public byte[] PasswordEncrypted { get; set; }
        public string ClientID { get; set; }

        public string ClientGUID { get; set; }
        public string EndPoint { get; set; }
        public int locationID { get; set; }
    }

    public class UserLoginData
    {
        public bool isValidUser { get; set; }
        public HubRiseCutomer User { get; set; }
        public List<HubRiseCutomerAddress> DeliveryAddresses { get; set; }
        public List<Object> PreviousOrders { get; set; }
    }

    public class HubRiseCallBackCustomer
    {
        public string id { get; set; }
        public string customer_list_id { get; set; }
        public string private_ref { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string gender { get; set; }
        public string birth_date { get; set; }
        public string company_name { get; set; }
        public string phone { get; set; }
        public string address_1 { get; set; }
        public string address_2 { get; set; }
        public string postal_code { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string delivery_notes { get; set; }
        public bool sms_marketing { get; set; }
        public bool email_marketing { get; set; }
        public int nb_orders { get; set; }
        public string order_total { get; set; }
        public bool isSSk { get; set; }
        public string password { get; set; }
        public DateTime? first_order_date { get; set; }
        public DateTime? last_order_date { get; set; }
        //public List<HubRiseCallBackLoyaltyCard> loyalty_cards { get; set; }
        public object custom_fields { get; set; } // Not known structure CustomerCustomFields
    }
}

