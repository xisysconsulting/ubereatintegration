﻿using HubRise.Models.EposAdmin;
using Microsoft.EntityFrameworkCore;

namespace Common.EposAdmin
{
    public partial class EposAdminContext : DbContext
    {

        public EposAdminContext(DbContextOptions<EposAdminContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Branch> Branches { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Company> Companies { get; set; }

        // Unable to generate entity type for table 'dbo.ServiceException'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                //   optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=EposAdmin;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Branch>(entity =>
            {
                entity.HasKey(e => new { e.LocationId, e.CompanyId });

                entity.Property(e => e.LocationId).HasColumnName("LocationID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");


                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

            });

            modelBuilder.Entity<Client>(entity =>
            {
                entity.HasKey(e => e.ClientId);

                entity.Property(e => e.ClientId)
                    .HasColumnName("ClientID")
                    .ValueGeneratedNever();


                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.ComputerName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.LocationId).HasColumnName("LocationID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.HasKey(e => e.CompanyId);

                entity.Property(e => e.CompanyId)
                    .HasColumnName("CompanyID")
                    .ValueGeneratedNever();


                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

               
            });

        }
    }
}