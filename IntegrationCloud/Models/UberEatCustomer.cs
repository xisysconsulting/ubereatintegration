﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UberEatClient.Models
{
    public class UberEatCustomer
    {
        public string errorText { get; set; }
        public bool isSSk { get; set; }
        public string customer_Email { get; set; }
        public string customer_Password { get; set; }
        public string phone { get; set; }
        public string lastName { get; set; }
    }
}
