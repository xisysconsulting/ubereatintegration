﻿using System;

namespace HubRise.Models.EposAdmin
{
    public partial class Client
    {
        public Client()
        {
            //ClientLicences = new HashSet<ClientLicences>();
        }

        public Guid ClientId { get; set; }
        public short LocationId { get; set; }
        public Guid CompanyId { get; set; }
        public string ComputerName { get; set; }
        public string Name { get; set; }
        
    }
}