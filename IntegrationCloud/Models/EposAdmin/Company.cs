﻿using System;
using System.Collections.Generic;

namespace HubRise.Models.EposAdmin
{
    public partial class Company
    {
        public Company()
        {
            //Branches = new HashSet<Branch>();
            //CompaniesReports = new HashSet<CompaniesReports>();
            //WebPortalUsers = new HashSet<WebPortalUsers>();
        }

        public Guid CompanyId { get; set; }
        public string Name { get; set; }
        public string ContactName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string WidgetClientId { get; set; }
    }
}