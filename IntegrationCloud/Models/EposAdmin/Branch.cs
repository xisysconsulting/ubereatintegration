﻿using System;
using System.Collections.Generic;

namespace HubRise.Models.EposAdmin
{
    public partial class Branch
    {
        public Branch()
        {
            //Clients = new HashSet<Client>();
        }

        public short LocationId { get; set; }
        public Guid CompanyId { get; set; }
        public string Name { get; set; }
  
    }
}