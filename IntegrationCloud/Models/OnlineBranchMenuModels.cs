﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubRise.Models
{

    public class BranchMenu
    {
        public List<Category> Categories { get; set; }
        public List<TakeawayDeal> Deals { get; set; }
    }
    public class TakeawayDeal
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public decimal Price { get; set; }
        public List<TakeAwayDealProductGroups> ProductGroups { get; set; }
    }
    public class TakeAwayDealProductGroups
    {
        public string Name { get; set; }
        public int ProductGroupID { get; set; }
        public int Number { get; set; }
        public string Weblabel { get; set; }
        public int WeblabelOrder { get; set; }
        public List<ProductVariantIDSet> ProductVariants { get; set; }
    }
    public class ProductVariantIDSet
    {
        public int ProductID { get; set; }
        public int VariantID { get; set; }
    }
    public partial class Category
    {
        public int CategoryID { get; set; }
        public List<TakeawayProduct> Products { get; set; }
        public int CategoryAID { get; set; }
        public int CategoryLID { get; set; }
        public string Name { get; set; }
        public object Position { get; set; }
        public object TimeStamp { get; set; }
        public bool IsArchived { get; set; }
    }
    public class TakeawayProduct   // this contains all data needed for product page including options/sizes
    {
        public string AppDescription { get; set; }
        public byte ProductType { get; set; }
        public int ProductID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> ImageUrls { get; set; }
        public bool IsVegetarian { get; set; }
        public decimal SinglePrice { get; set; }
        public List<WebListItem> Sizes { get; set; }
        public List<WebProductOption> Options { get; set; }
        public List<ModifierHeader> ModifierHeaders { get; set; }
        public List<WebListItem> Modifiers { get; set; }
        public bool SingleModifier { get; set; }
        public bool AllowNoModifier { get; set; }
    }
    public class WebListItem
    {
        public byte ProductType { get; set; }
        public string Text { get; set; }
        public int ItemID { get; set; }
        public decimal Price { get; set; }
        public string Header { get; set; }
        public bool ShowClose { get; set; }
        public int? ForceNumber { get; set; }
        public int VariantID { get; set; }
        public List<WebListItem> Sizes { get; set; }
    }
    public class ModifierHeader
    {
        public string Header { get; set; }
        public bool ShowClose { get; set; }
        public int? ForceNumber { get; set; }
    }
    public class WebProductOption
    {
        public string Text { get; set; }
        public int ItemID { get; set; }
        public List<WebListItem> OptionItems { get; set; }
    }
}
