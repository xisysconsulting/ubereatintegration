﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UberEatClient.Models
{

    public class UberEatOrder
    {
        public EaterInfo eater_info { get; set; }
        public string store_id { get; set; }
        public string order_num { get; set; }
        public string created_time { get; set; }
        public string current_state { get; set; }
        public string currency_code { get; set; }
        public List<UberOrderItem> order_items { get; set; }
        public List<Charge> charges { get; set; }
    }

    public class EaterInfo
    {
        public string first_name { get; set; }
    }

    public class UberSelectedOption
    {
        public string option_id { get; set; }
        public string external_data { get; set; }
        public int quantity { get; set; }
        public string price { get; set; }
    }

    public class UberOrderItem
    {
        public string item_id { get; set; }
        public string external_data { get; set; }
        public int quantity { get; set; }
        public string price { get; set; }
        public List<UberSelectedOption> selected_options { get; set; }
    }

    public class Charge
    {
        public string charge_type { get; set; }
        public string price { get; set; }
        public string formatted_price { get; set; }
    }

    public class UberEatWebhootNotification
    {
        public string event_id { get; set; }
        public string resource_href { get; set; }
        public OrderMeta meta { get; set; }
        public string event_type { get; set; }
        public string event_time { get; set; }
    }
    public class OrderMeta
    {
        public string status { get; set; }
        public object rider_id { get; set; }
        ////public string rider_id { get; set; } Commented 26/06/2019, not used and was throwing error
        public string user_id { get; set; }
        public string resource_id { get; set; }
    }

    public class OrderAccept
    {
        public string reason { get; set; }
    }

    public class UberReason
    {
        public string explanation { get; set; }
    }

    public class OrderDenial
    {
        public UberReason reason { get; set; }
    }
}
