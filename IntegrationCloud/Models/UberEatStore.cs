﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UberEatClient.Models
{
    public class UberEatStore
    {
        public string status { get; set; }
        public string name { get; set; }
        public string store_id { get; set; }
        public int avg_prep_time { get; set; }
        public object partner_store_id { get; set; }
        public UbereatStoreLocation location { get; set; }
        public List<object> contact_emails { get; set; }
        public string price_bucket { get; set; }
        public string raw_hero_url { get; set; }
    }
    public class UberEatStoreStatus
    {
        public bool auto_accept_enabled { get; set; }
        public string store_id { get; set; }
        public object online_status { get; set; }
        public UbereatPosMetadata pos_metadata { get; set; }
        public bool pos_integration_enabled { get; set; }
        public bool order_release_enabled { get; set; }
    }
    public class UbereatPosMetadata
    {
    }
    public class UbereatStoreLocation
    {
        public string city { get; set; }
        public string country { get; set; }
        public double longitude { get; set; }
        public string state { get; set; }
        public string postal_code { get; set; }
        public object address_2 { get; set; }
        public string address { get; set; }
        public double latitude { get; set; }
    }

    public class StoreStatusRequest
    {
        public bool pos_integration_enabled { get; set; }
        public bool order_release_enabled { get; set; }
    }

    public class StoreErrorResponse
    {
        public string code { get; set; }
        public string message { get; set; }
    }
}
