﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UberEatClient.Models
{
    public class UberEatNotification
    {
        public long UberEatNotificationId { get; set; }
        public string EventId { get; set; }
        public string EventTime { get; set; }
        public string EventType { get; set; }
        public string Meta { get; set; }
        public string ResourceHref { get; set; }
        public bool IsOrderProcess { get; set; }
        public int Count { get; set; }
        public string Status { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ProcessDate { get; set; }
    }
}
