﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HubRise.Models
{
    public class BranchSettings
    {
        public Details Details { get; set; }
        public List<DeliveryType> DeliveryTypes { get; set; }
        public HalfPriceSchedule HalfPriceSchedule { get; set; }
        public string ComoApiKey { get; set; }

        public License License { get; set; }
        public List<Language> Languages { get; set; }
        public List<string> PaymentMethods { get; set; }
        public List<AngularAnimation> AngularAnimations { get; set; }
        public PaymentSettings PaymentSettings { get; set; }
    }
    public class CompanySettings
    {
        public string GoogleKey { get; set; }
        public string GoogleRegion { get; set; }
        public string UITheme { get; set; }
        public string Title { get; set; }
        public string homeImage { get; set; } = "Pepelogo172611";
        public string authImage { get; set; } = "Pepelogo172611";
        public bool Delivery { get; set; }
        public bool Collection { get; set; }

    }

    public class License
    {
        public int NoOfUsers { get; set; }
        public DateTime Expiry { get; set; }
    }

    public class Language
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Icon { get; set; }
        public string Currency { get; set; }
        public string CurrencySymbol { get; set; }
    }

    public class AngularAnimation
    {
        public string Key { get; set; }
        public string Direction { get; set; }
        public string Dtl1 { get; set; }
    }

    public class Address
    {
        public string OutMailServer { get; set; }
        public string MailServerUserName { get; set; }
        public string MailServerPassword { get; set; }
        public int OutMailPort { get; set; }
        public bool OutMailUserSSL { get; set; }
        public bool IsBillingAddress { get; set; }
        public int AddressID { get; set; }
        public string AddressName { get; set; }
        public string RecipientTitle { get; set; }
        public string RecipientFirstName { get; set; }
        public string RecipientSurname { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string TownOrCity { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public string Postcode { get; set; }
        public string ContactNumber { get; set; }
        public double Distance { get; set; }
        public bool IsMusicVenue { get; set; }
    }

    public class Details
    {
        public Address Address { get; set; }
        public string ImageUrl { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }

    public class DeliverySlot
    {
        public int DeliverySlotAID { get; set; }
        public int DeliverySlotLID { get; set; }
        public int DeliveryTypeAID { get; set; }
        public int DeliveryTypeLID { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }
        public bool Sunday { get; set; }
        public bool IsArchived { get; set; }
    }

    public class DeliveryType
    {
        public object PendingDeliverySlotDeletions { get; set; }
        public int DeliveryTypeAID { get; set; }
        public int DeliveryTypeLID { get; set; }
        public string Name { get; set; }
        public double? FixedPrice { get; set; }
        public double? PricePerMile { get; set; }
        public string MaxWeightKg { get; set; }
        public string MinWeightKg { get; set; }
        public double? MaxDistanceMiles { get; set; }
        public double? MinAmount { get; set; }
        public string MaxAmount { get; set; }
        public bool IsPickup { get; set; }
        public bool IsArchived { get; set; }
        public bool IsDefault { get; set; }
        public int? WaitTimeMinutes { get; set; }
        public int LocationID { get; set; }
        public List<DeliverySlot> DeliverySlots { get; set; }
    }

    public class HalfPriceSchedule
    {
        public int LocationID { get; set; }
        public object StartTime1 { get; set; }
        public object EndTime1 { get; set; }
        public object StartTime2 { get; set; }
        public object EndTime2 { get; set; }
        public bool HalfPriceNow { get; set; }
        public int MinTimeBetweenUpdates { get; set; }
        public object TimeLimit { get; set; }
    }

    public class PaymentSettings
    {
        public string WordPayClientkey { get; set; }
        public string WordPayServerkey { get; set; }
        public string PayPalSandboxKey { get; set; }
        public string PayPalProductionKey { get; set; }
        public string PayPalEnvr { get; set; }
    }

    public class ImageData
    {
        public string name { get; set; }
        public string Id { get; set; }
        public string @type { get; set; }
        public string md5 { get; set; }
        public byte[] binary { get; set; }
    }
}
