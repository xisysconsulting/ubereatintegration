﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UberEatClient.Models
{
    public class UberEatMenu
    {
        public List<Section> sections { get; set; }
    }
    public class TimePeriod
    {
        public string start_time { get; set; }
        public string end_time { get; set; }
    }

    public class ServiceAvailability
    {
        public string day_of_week { get; set; }
        public bool enabled { get; set; }
        public List<TimePeriod> time_periods { get; set; }
    }
    public class CustomizationOption
    {
        public string title { get; set; }
        public double price { get; set; }
        public string external_id { get; set; }
    }

    public class Customization
    {
        public string title { get; set; }
        public int min_permitted { get; set; }
        public int max_permitted { get; set; }
        public List<CustomizationOption> customization_options { get; set; }
    }
    public class UberItem
    {
        public object suspend_until { get; set; }
        public bool disable_instructions { get; set; }
        public string title { get; set; }
        public double price { get; set; }
        public string currency_code { get; set; }
        public string external_id { get; set; }
        public string item_description { get; set; }
        public bool? is_alcohol { get; set; }
        public double tax_rate { get; set; }
        public string image_url { get; set; }
        public List<Customization> customizations { get; set; }
        public int? vat_rate_percentage { get; set; }

        public string item_id { get; set; }

        public NutritionalInfo nutritional_info { get; set; }


    }

    public class Subsection
    {
        public string subsection_id { get; set; }
        public string title { get; set; }
        public List<UberItem> items { get; set; }
    }

    public class NutritionalInfo
    {
        public List<string> allergens { get; set; }
    }

    public class Section
    {
        public string title { get; set; }
        public string subtitle { get; set; }
        public string section_id { get; set; }
        public List<ServiceAvailability> service_availability { get; set; }
        public List<Subsection> subsections { get; set; }
    }


}
